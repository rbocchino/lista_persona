package es.devcenter.talent.campus.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ListaPersonaTest {

	private List<Persona> lista;
	
	@Mock
	private PersonaWriter personaWriter;
	
	@Mock
	private PersonaReader personaReader;
	
	@Mock
	private Config config;
	
	@InjectMocks
	private ListaPersona listaPersona;
	
	@Before
	public void init() {
		this.lista = new ArrayList<Persona>();
		this.lista.add(new Persona("X9964870C", "nombre 1", "apellidos 1", 34));
		this.lista.add(new Persona("00000000T", "nombre 2", "apellidos 2", 23));
		
		this.listaPersona.setPersonas(this.lista);
	}
	
	@Test
	public void testFind() {
		Assert.assertNull("Dni nulo", this.listaPersona.find(null));
		Assert.assertNull("No se esperaba encontrar este dni", this.listaPersona.find("1111T"));
		
		Assert.assertEquals(new Persona("00000000T", "nombre 2", "apellidos 2", 23), this.listaPersona.find("00000000T"));
		Assert.assertEquals(new Persona("00000000T", "nombre 2", "apellidos 2", 23), this.listaPersona.find("  00000000t  "));
	}


	@Test(expected = ListaPersonaException.class)
	public void testAddDniIncorrecto() throws ListaPersonaException {
		this.listaPersona.add(new Persona("ABC", "nombre 3", "apellidos 3", 45));
	}
	
	@Test(expected = ListaPersonaException.class)
	public void testAddDniNulo() throws ListaPersonaException {
		this.listaPersona.add(new Persona(null, "nombre 3", "apellidos 3", 45));
	}

	@Test
	public void testAdd() throws ListaPersonaException {
		this.listaPersona.add(new Persona("X1234567C", "nombre 3", "apellidos 3", 45));
		Assert.assertEquals(3, this.lista.size());		
		Assert.assertEquals(new Persona("X1234567C", "nombre 3", "apellidos 3", 45), this.listaPersona.find("X1234567C"));
	}
	
	@Test
	public void testRemove() {
		this.listaPersona.remove("");
		Assert.assertEquals(2, this.lista.size());
		
		this.listaPersona.remove("12345678P");
		Assert.assertEquals(2, this.lista.size());

		this.listaPersona.remove("00000000T");
		Assert.assertEquals(1, this.lista.size());
		
		Assert.assertNull("Not found expected", this.listaPersona.find("00000000T"));
	}
	
	@Test
	public void testCheckDni() { 
		Assert.assertFalse(this.listaPersona.isValidDni("12345678U7"));
		Assert.assertFalse(this.listaPersona.isValidDni("ABC"));
		
		Assert.assertTrue("Es un Dni valido", this.listaPersona.isValidDni("00000000T"));
		Assert.assertTrue("Es un Nie valido", this.listaPersona.isValidDni("X1234567C"));
	}
	
	@Test
	public void testLoad() throws IOException, ListaPersonaException {
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(new Persona("E1234567U", "nombre moke", "apellidos moke", 56));
		
		Mockito.when(this.personaReader.read(Mockito.anyString())).thenReturn(personas);
		Mockito.when(this.config.getCsvPath()).thenReturn("csv");
		
		this.listaPersona.load();
		
		Mockito.verify(this.personaReader, Mockito.times(1)).read("csv");
	}
	
	@Test(expected = ListaPersonaException.class)
	public void testLoadException() throws IOException, ListaPersonaException {
		Mockito.when(this.personaReader.read(Mockito.anyString())).thenThrow(new IOException("Error moke IO"));
		Mockito.when(this.config.getCsvPath()).thenReturn("csv");
		
		this.listaPersona.load();
	}
	
	@Test
	public void testSave() throws IOException, ListaPersonaException {
		Mockito.doAnswer(invocation -> {
			return null;
		}).when(this.personaWriter).write(Mockito.eq("csv"), Mockito.any());
		
		Mockito.when(this.config.getCsvPath()).thenReturn("csv");
		
		this.listaPersona.save();
		
		Mockito.verify(this.personaWriter, Mockito.times(1)).write("csv", this.lista);
	}
	
	@Test(expected = ListaPersonaException.class)
	public void testSaveException() throws IOException, ListaPersonaException {
		Mockito.doAnswer(invocation -> {
			throw new IOException("Error moke IO");
		}).when(this.personaWriter).write(Mockito.eq("csv"), Mockito.any());
		
		Mockito.when(this.config.getCsvPath()).thenReturn("csv");
		
		this.listaPersona.save();
	}
}
