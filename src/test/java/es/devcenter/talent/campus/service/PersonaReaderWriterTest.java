package es.devcenter.talent.campus.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;


public class PersonaReaderWriterTest {

	@Test
	public void testReaderWriter() throws IOException {
		List<Persona> list = new ArrayList<Persona>();
		list.add(new Persona("X9964870C", "nombre 1", "apellidos 1", 34));
		list.add(new Persona("00000000T", "nombre 2", "apellidos 2", 23));
		
		String csv = System.getProperty("java.io.tmpdir") + File.separator + "persona.csv";
		
		File file = new File(csv);
		file.delete();
		
		PersonaWriter personaWriter = new PersonaWriter();
		personaWriter.write(csv, list);
		
		PersonaReader personaReader = new PersonaReader();
		List<Persona> actual =  personaReader.read(csv);
		
		Assert.assertEquals(list, actual);
	}

}
