package es.devcenter.talent.campus.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PersonaReader {

	public List<Persona> read(String csv) throws IOException {
		List<Persona> list = new ArrayList<Persona>();
		try(FileReader fr = new FileReader(csv); BufferedReader br = new BufferedReader(fr)) {
			String line = "";
			while( (line = br.readLine()) != null) {
				String[] columnas = line.split(Constantes.CSV_SEPARATOR, 4);
				
				Persona persona = new Persona(columnas[0], columnas[1], columnas[2], Integer.parseInt(columnas[3]));
				list.add(persona);
			}
		}
		
		return list;
	}

}
