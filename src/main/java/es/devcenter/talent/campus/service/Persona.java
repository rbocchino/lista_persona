package es.devcenter.talent.campus.service;

public class Persona {

	private String dni;

	private String nombre;

	private String apellidos;

	private int edad;

	public Persona(String dni, String nombre, String apellidos, int edad) {
		super();
		this.dni = dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
	}

	public String getDni() {
		return this.dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return this.edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		if (this.apellidos == null) {
			if (other.apellidos != null)
				return false;
		} else if (!this.apellidos.equals(other.apellidos))
			return false;
		if (this.dni == null) {
			if (other.dni != null)
				return false;
		} else if (!this.dni.equals(other.dni))
			return false;
		if (this.edad != other.edad)
			return false;
		if (this.nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!this.nombre.equals(other.nombre))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Persona [dni=" + this.dni + ", nombre=" + this.nombre + ", apellidos=" + this.apellidos + ", edad="
				+ this.edad + "]";
	}

}
