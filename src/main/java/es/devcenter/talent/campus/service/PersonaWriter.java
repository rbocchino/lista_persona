package es.devcenter.talent.campus.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class PersonaWriter {

	public void write(String csv, List<Persona> personas) throws IOException {
		try (FileWriter fileWriter = new FileWriter(csv); 
				BufferedWriter bw = new BufferedWriter(fileWriter)) {
			for (Persona persona : personas) {
				String line = persona.getDni() + Constantes.CSV_SEPARATOR + persona.getNombre()
						+ Constantes.CSV_SEPARATOR + persona.getApellidos() + Constantes.CSV_SEPARATOR + persona.getEdad() + "\n";
				bw.write(line);
			}
		}
	}
}
