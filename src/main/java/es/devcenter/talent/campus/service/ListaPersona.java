package es.devcenter.talent.campus.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class ListaPersona {

	private List<Persona> personas;
	
	private PersonaWriter personaWriter;
	
	private PersonaReader personaReader;
	
	private Config config;
	
	public ListaPersona(PersonaWriter personaWriter, PersonaReader personaReader, Config config) {
		this.personas = new ArrayList<>();
		this.personaWriter = personaWriter;
		this.personaReader = personaReader;	
		this.config = config;
	}
	
	protected void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}

	public Persona find(String dni) {
		if(dni == null) {
			return null;
		}
		
		String trimDni = dni.trim();
		for(Persona persona : this.personas) {
			if(persona.getDni().equalsIgnoreCase(trimDni)) {
				return persona;
			}
		}
		
		return null;
	}
	
	public void add(Persona persona) throws ListaPersonaException {
		if(persona.getDni() == null) {
			throw new ListaPersonaException("El dni es nulo");
		}
		
		String trimDni = persona.getDni().trim().toUpperCase();		
		if( ! this.isValidDni(trimDni) ) {
			throw new ListaPersonaException(String.format("El dni %s no es valido.", trimDni));			
		}
		
		if( this.find(trimDni) != null ) {
			throw new ListaPersonaException(String.format("Ya existe una persona con el dni %s", trimDni));
		}
		
	    this.personas.add(persona);	
	}

	protected boolean isValidDni(String trimDni) {
		Pattern patronDni = Pattern.compile("^[A-Z0-9][0-9]{7}[A-Z]$");
		return patronDni.matcher(trimDni).find();
	}
	
	public void remove(String dni) {
		if(dni == null) {
			return;
		}
		
		Persona persona = this.find(dni.trim());
		if(persona != null) {
			this.personas.remove(persona);
		}
	}
	
	public void print() {
		if(this.personas.isEmpty()) {
			System.out.println("Empty list");
			return;
		}
		
		for(Persona persona : this.personas) {
			System.out.println(persona);
		}
	}

	public void load() throws ListaPersonaException {
		try {
			this.personas = this.personaReader.read(this.config.getCsvPath());
			System.out.println(String.format("Cargadas %s personas", this.personas.size()));
		} catch (IOException e) {
			throw new ListaPersonaException("Error al leer el fichero " + this.config.getCsvPath(), e);
		}
	}
	
	public void save() throws ListaPersonaException {
		try {
			this.personaWriter.write(this.config.getCsvPath(), this.personas);
			System.out.println("Personas exportadas correctamente en " + this.config.getCsvPath());
		} catch (IOException e) {
			throw new ListaPersonaException("Error al guardar el fichero " + this.config.getCsvPath(), e);
		}
	}
}
