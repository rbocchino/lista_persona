package es.devcenter.talent.campus.service;

public class ListaPersonaException extends Exception {

	private static final long serialVersionUID = 1614083561966379053L;

	public ListaPersonaException(String message) {
		super(message);
	}

	public ListaPersonaException(String message, Throwable cause) {
		super(message, cause);
	}

}
