package es.devcenter.talent.campus.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {

	private Properties properties;
	
	public Config() throws IOException {
		
		Properties prop = new Properties();
		ClassLoader classLoader = Config.class.getClassLoader();
		InputStream is = classLoader.getResourceAsStream("application.properties");
		prop.load(is);
		
		this.properties = prop;
	}
	
	public String getCsvPath() {
		return this.properties.getProperty("csv.path");
	}

}
