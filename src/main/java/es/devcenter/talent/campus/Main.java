package es.devcenter.talent.campus;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

import es.devcenter.talent.campus.service.Config;
import es.devcenter.talent.campus.service.ListaPersona;
import es.devcenter.talent.campus.service.ListaPersonaException;
import es.devcenter.talent.campus.service.Persona;
import es.devcenter.talent.campus.service.PersonaReader;
import es.devcenter.talent.campus.service.PersonaWriter;

public class Main {

	public static void main(String[] args) throws IOException {
		ListaPersona listaPersona = new ListaPersona(new PersonaWriter(), new PersonaReader(), new Config());
		principalCycle(listaPersona);
	}

	private static void principalCycle(ListaPersona listaPersona) {
		try (Scanner scanner = new Scanner(System.in)) {
			
			System.out.println("Bienvenido al gestor de personas.");
			
			boolean continuar = true;
			while(continuar) {
				System.out.println("¿Qué opción quieres elegir?\n"
						+ "1.- Cargar en memoria una lista de persona\n"
						+ "2.- Guardar en el disco duro la lista de personas\n"
						+ "3.- Crear una nueva persona\n"
						+ "4.- Imprimir la lista de personas\n"
						+ "5.- Salir.\n"
						+ "Escribe tu selección y pulsa la tecla INTRO:");
				
				int opcion = readOption(scanner);
				
				switch (opcion) {
				case 1:
					try {
						listaPersona.load();
					} catch (ListaPersonaException e) {
						System.out.println(e.getMessage());
					}
					System.out.println();
					break;					
				case 2:
					try {
						listaPersona.save();
					} catch (ListaPersonaException e) {
						System.out.println(e.getMessage());
					}
					System.out.println();
					break;
				case 3:
					insertarPersona(scanner,listaPersona);
					System.out.println();
					break;
				case 4:
					System.out.println("Listado de personas:");
					listaPersona.print();
					System.out.println();
					break;
				case 5:
					continuar = false;
					System.out.println("Adios!!");
					break;
				default:
					System.out.println("Opcion no valida");
					break;
				}
				
			}
		}
	}

	private static int readOption(Scanner scanner) {
		int opcion = -1;
		try {
			opcion = nextInt(scanner);
		} catch(InputMismatchException e) {
			System.out.println(e.getMessage());
		}
		return opcion;
	}

	private static int nextInt(Scanner scanner) {
		int opcion = scanner.nextInt();
		scanner.nextLine();		
		return opcion;
	}

	private static void insertarPersona(Scanner scanner, ListaPersona listaPersona) {
		boolean error;

		do {			
			try {
				error = false;
				System.out.println("Nueva persona:");
				System.out.print("DNI: ");
				String dni = scanner.nextLine();
				System.out.print("Nombre: ");
				String nombre = scanner.nextLine();
				System.out.print("Apellidos: ");
				String apellidos = scanner.nextLine();
				System.out.print("Edad: ");
				int edad = nextInt(scanner);
				
				Persona persona = new Persona(dni, nombre, apellidos, edad);
				listaPersona.add(persona);
				System.out.println("Persona añadida correctamente.");
			} catch (ListaPersonaException e) {
				System.out.println(e.getMessage());
				error = true;
			} catch (InputMismatchException e) {
				System.out.println(e.getMessage());
				error = true;
			}
			System.out.println();
		} while(error);
	}
}
